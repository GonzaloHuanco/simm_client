var spartanServices = angular.module('spartanServices', ['ngResource']);

spartanServices.factory('Inventory', ['$resource',
  function($resource){
    return $resource('http://127.0.0.1:8000/inventario/elemento/:elementId.json', {}, {
      query: {method:'GET',params:{elementId:'@id'},isArray:true}
    });
  }]);
//http://127.0.0.1:8000/inventario/elemento/.json
//http://restcountries.eu/rest/v1/all

//params:{phoneId:'phones'}
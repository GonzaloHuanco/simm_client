

/* App Module */
var spartanApp = angular.module('spartanApp', [
  'ngRoute',
  'spartanServices',
  'spartanControllers',
  'ngMaterial',
  'angular-carousel'
]);

spartanApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/inventory', {
        templateUrl: 'views/inventory/index.html',
        controller: 'InventoryCtrl'
      }).
      when('/inventory/new', {
        templateUrl: 'views/inventory/add.html',
        controller: 'AddController'
      }).
      when('/inventory/:id/update', {
        templateUrl: 'views/inventory/update.html',
        controller: 'EditController'
      }).
      when('/inventory/:id/view', {
        templateUrl: 'views/inventory/view.html',
        controller: 'ViewController'
      }).
      when('/login', {
        templateUrl: 'views/login/index.html',
        controller: 'LoginController'
      }).
      when('/faq', {
        templateUrl: 'views/faq/index.html',
        controller: 'FaqController'
      }).
      when('/registrarse', {
        templateUrl: 'views/login/registrarse.html',
        controller: 'RegistrarseController'
      }).
      when('/acercaDe', {
        templateUrl: 'views/acercaDe/index.html',
        controller: 'AcercaDeController'
      }).
      otherwise({
        redirectTo: '/inventory'
      });
  }]);


spartanApp.controller('FaqController',function($scope){
    var imagePath = '/simm_client/assets/images/monitor.png';
    //$mdThemingProvider.setDefaultTheme('altTheme');
     // Specify a list of font-icons with ligatures and color overrides
      var iconData = [
            {name: 'accessibility'  , color: "#777" },
            {name: 'question_answer', color: "rgb(89, 226, 168)" },
            {name: 'backup'         , color: "#A00" },
            {name: 'email'          , color: "#00A" }
          ];
      $scope.fonts = [].concat(iconData);
      // Create a set of sizes...
      $scope.sizes = [
        {size:"md-18",padding:0},
        {size:"md-24",padding:2},
        {size:"md-36",padding:6},
        {size:"md-48",padding:10}
      ];
})

spartanApp.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);


spartanApp.controller('RegistrarseController', ['$scope','$http', function($scope, $http) {

    $scope.user = {};
    $scope.registrarUser = function(){
      console.log("estos son los datos del usuario ->",$scope.user);
      /*$http.post('http://127.0.0.1:8000/inventario/elemento', $scope.gymInventory).
          success(function(data, status, headers, config) {
            $location.path('inventory/'+data.id+'/view');
          }).
          error(function(data, status, headers, config) {
            console.log("error ->",data);
          });*/
    }
    //$scope.detail = Inventory.get({codeId: 7}, function(detail) {
    //    console.log("Esto es lo que esta devolviendo esta mierda ->",detail)
    //}); 
    
    
}]);

spartanApp.controller('AcercaDeController', ['$scope','$http', function($scope, $http) {

    $scope.user = {};
    $scope.registrarUser = function(){
      console.log("estos son los datos del usuario ->",$scope.user);
      /*$http.post('http://127.0.0.1:8000/inventario/elemento', $scope.gymInventory).
          success(function(data, status, headers, config) {
            $location.path('inventory/'+data.id+'/view');
          }).
          error(function(data, status, headers, config) {
            console.log("error ->",data);
          });*/
    }
    //$scope.detail = Inventory.get({codeId: 7}, function(detail) {
    //    console.log("Esto es lo que esta devolviendo esta mierda ->",detail)
    //}); 
    
    
}]);

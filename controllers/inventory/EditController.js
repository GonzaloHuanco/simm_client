

angular.module('spartanControllers').controller('EditController', ['$scope', '$http','$routeParams','Inventory','$location', function($scope, $http,$routeParams,Inventory,$location){
    $scope.gymInventory = {};
    Inventory.get({elementId: $routeParams.id}, function(element) {
      $scope.gymInventory = element;
    });
    $scope.editElement = function(){
      $http.put('http://127.0.0.1:8000/inventario/elemento/'+$routeParams.id, $scope.gymInventory)
      .success(function(element){
        if (element.length != '') {
           $location.url('/#'); 
        };
      });
      
    };
}]);
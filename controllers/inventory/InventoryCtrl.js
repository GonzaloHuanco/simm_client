var spartanControllers = angular.module('spartanControllers', []);

spartanControllers.controller('InventoryCtrl', ['$scope','$http','Inventory','elementForm','$route', function($scope, $http,Inventory,elementForm,$route) {
    
    $scope.inventory_list = Inventory.query();
    
    $scope.deleteElement = function(id){
        $http.delete('http://127.0.0.1:8000/inventario/elemento/'+id).success(function(element){
          $route.reload();
        }); 
    };
}]);



/*phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams',
  function($scope, $routeParams) {
    $scope.phoneId = $routeParams.phoneId;
  }]);*/
//forma que utiliza un array para inyectar dependencias
angular
  .module('spartanControllers')
  .controller('AddController', ['$scope', '$http','Inventory','$location', function($scope, $http, Inventory,$location){
    $scope.gymInventory = {};
    $scope.band = false;

    $scope.saveElement = function(){
        $http.post('http://127.0.0.1:8000/inventario/elemento', $scope.gymInventory).
          success(function(data, status, headers, config) {
            $location.path('inventory/'+data.id+'/view');
          }).
          error(function(data, status, headers, config) {
            console.log("error ->",data);
          });
    }
}]);
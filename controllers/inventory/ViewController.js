angular.module('spartanControllers').controller('ViewController', ['$scope', '$http','$routeParams','Inventory','$location', function($scope, $http,$routeParams,Inventory,$location){
    $scope.gymInventory = {};
    Inventory.get({elementId: $routeParams.id}, function(element) {
      $scope.gymInventory = element;
    });
}]);